# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the missioncenter package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: missioncenter\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-11-27 16:36+0200\n"
"PO-Revision-Date: 2023-12-07 09:23+0000\n"
"Last-Translator: Jiri Grönroos <jiri.gronroos@iki.fi>\n"
"Language-Team: Finnish <https://hosted.weblate.org/projects/mission-center/"
"mission-center/fi/>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.3-dev\n"

#: data/io.missioncenter.MissionCenter.desktop.in:3
#: data/io.missioncenter.MissionCenter.metainfo.xml.in:4
msgid "Mission Center"
msgstr "Tehtäväkeskus"

#: data/io.missioncenter.MissionCenter.desktop.in:10
msgid ""
"Task manager;Resource monitor;System monitor;Processor;Processes;Performance "
"monitor;CPU;GPU;Disc;Disk;Memory;Network;Utilisation;Utilization"
msgstr ""
"Task manager;Resource monitor;System monitor;Processor;Processes;Performance "
"monitor;CPU;GPU;Disc;Disk;Memory;Network;Utilisation;Utilization;Mission "
"Center;tehtäväkeskus;resurssit;suoritin;prosessit;suorituskyky;näytönohjain;"
"levy;muisti;verkko"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:5
msgid "Mission Center Developers"
msgstr "Mission Center -kehittäjät"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:14
msgid "Monitor system resource usage"
msgstr "Tarkkaile järjestelmän resurssien käyttöä"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:16
msgid "Monitor your CPU, Memory, Disk, Network and GPU usage"
msgstr ""
"Tarkkaile suorittimen, muistin, levyn, verkon ja näytönohjaimen käyttöä"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:17
msgid "Features:"
msgstr "Ominaisuudet:"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:19
msgid "Monitor overall or per-thread CPU usage"
msgstr "Tarkkaile suorittimen käyttöä yleisesti tai säiekohtaisesti"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:20
msgid ""
"See system process, thread, and handle count, uptime, clock speed (base and "
"current), cache sizes"
msgstr ""
"Näe järjestelmän prosessi-, säie- ja kahvamäärät, toiminta-aika, "
"kellotaajuus (perustaajuus ja nykyinen taajuus) ja välimuistien koot"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:21
msgid "Monitor RAM and Swap usage"
msgstr "Tarkkaile keskusmuistin ja swapin käyttöä"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:22
msgid "See a breakdown how the memory is being used by the system"
msgstr "Näe erittely, miten järjestelmä käyttää muistia"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:23
msgid "Monitor Disk utilization and transfer rates"
msgstr "Tarkkaile levyn käyttöä ja siirtonopeuksia"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:24
msgid "Monitor network utilization and transfer speeds"
msgstr "Tarkkaile verkon käyttöä ja siirtonopeuksia"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:25
msgid ""
"See network interface information such as network card name, connection type "
"(Wi-Fi or Ethernet), wireless speeds and frequency, hardware address, IP "
"address"
msgstr ""
"Näe verkkoliitäntöjen tiedot kuten verkkokortin nimi, yhteyden tyyppi (Wi-Fi "
"tai Ethernet), langattoman verkon nopeus ja taajuus, laiteosoite sekä IP-"
"osoite"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:28
msgid ""
"Monitor overall GPU usage, video encoder and decoder usage, memory usage and "
"power consumption, powered by the popular NVTOP project"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:31
msgid "See a breakdown of resource usage by app and process"
msgstr "Näe erittely, miten sovellukset ja prosessit käyttävät resursseja"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:32
msgid "Supports a minified summary view for simple monitoring"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:33
msgid ""
"Use OpenGL rendering for all the graphs in an effort to reduce CPU and "
"overall resource usage"
msgstr ""
"Käytä OpenGL-piirtoa kaikilla kaavioille suoritinkäytön vähentämiseksi ja "
"yleisesti resurssien vähäisen käytön vuoksi"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:34
msgid "Uses GTK4 and Libadwaita"
msgstr "Käyttää GTK4:ää ja Libadwaitaa"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:35
msgid "Written in Rust"
msgstr "Kirjoitettu Rustilla"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:36
msgid "Flatpak first"
msgstr "Flatpak ensisijaisesti"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:38
msgid "Limitations (there is ongoing work to overcome all of these):"
msgstr "Rajoitukset (näiden kaikkien ratkaisemiseksi tehdään töitä):"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:40
msgid "No per-process network usage"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:41
msgid ""
"GPU support is experimental and only AMD and nVidia GPUs can be monitored"
msgstr ""
"Näytönohjaintuki on kokeellinen, ja vain AMD:n ja nVidian näytönohjaimia on "
"mahdollista tarkkailla"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:43
msgid "Comments, suggestions, bug reports and contributions welcome"
msgstr ""

#: data/io.missioncenter.MissionCenter.gschema.xml:30
msgid "Which page is shown on application startup"
msgstr "Sovelluksen käynnistyessä avattava sivu"

#: data/io.missioncenter.MissionCenter.gschema.xml:36
msgid "How fast should the data be refreshed and the UI updated"
msgstr "Kuinka nopeasti data päivitetään käyttöliittymään"

#: data/io.missioncenter.MissionCenter.gschema.xml:41
#: resources/ui/preferences/page.blp:38
msgid "Parent and child process stats are shown individually or merged upwards"
msgstr ""
"Prosessin ja sen lapsiprosessin tilastot näytetään yksittäin tai yhdistettynä"

#: data/io.missioncenter.MissionCenter.gschema.xml:46
msgid "Column sorting is persisted across app restarts"
msgstr "Sarakejärjestys säilyy sovelluksen uudelleenkäynnistysten välillä"

#: data/io.missioncenter.MissionCenter.gschema.xml:51
msgid "The column id by which the Apps page view is sorted"
msgstr ""

#: data/io.missioncenter.MissionCenter.gschema.xml:56
msgid "The sorting direction of the Apps page view"
msgstr ""

#: data/io.missioncenter.MissionCenter.gschema.xml:62
msgid "Which graph is shown on the CPU performance page"
msgstr ""

#: data/io.missioncenter.MissionCenter.gschema.xml:67
msgid "Show kernel times in the CPU graphs"
msgstr ""

#: data/io.missioncenter.MissionCenter.gschema.xml:72
msgid "Which page is shown on application startup, in the performance tab"
msgstr ""

#: resources/ui/performance_page/cpu.blp:44 src/apps_page/mod.rs:1168
#: src/performance_page/mod.rs:363
msgid "CPU"
msgstr "Suoritin"

#: resources/ui/performance_page/cpu.blp:76
#: resources/ui/performance_page/cpu_details.blp:46
#: resources/ui/performance_page/gpu_details.blp:44
msgid "Utilization"
msgstr "Käyttö"

#: resources/ui/performance_page/cpu.blp:86
msgid "100%"
msgstr "100 %"

#: resources/ui/performance_page/cpu.blp:142
msgid "Change G_raph To"
msgstr "Vaih_da kaavioksi"

#: resources/ui/performance_page/cpu.blp:145
msgid "Overall U_tilization"
msgstr "Käytt_ö yhteensä"

#: resources/ui/performance_page/cpu.blp:150
msgid "Logical _Processors"
msgstr "_Loogiset suorittimet"

#: resources/ui/performance_page/cpu.blp:156
msgid "Show Kernel Times"
msgstr "Näytä ydinajat"

#: resources/ui/performance_page/cpu.blp:163
#: resources/ui/performance_page/disk.blp:179
#: resources/ui/performance_page/gpu.blp:219
#: resources/ui/performance_page/memory.blp:152
#: resources/ui/performance_page/network.blp:121
msgid "Graph _Summary View"
msgstr "Kaavion _yhteenvedon näkymä"

#: resources/ui/performance_page/cpu.blp:168
#: resources/ui/performance_page/disk.blp:184
#: resources/ui/performance_page/gpu.blp:224
#: resources/ui/performance_page/memory.blp:157
#: resources/ui/performance_page/network.blp:126
msgid "_View"
msgstr "_Näytä"

#: resources/ui/performance_page/cpu.blp:171
#: resources/ui/performance_page/disk.blp:187
#: resources/ui/performance_page/gpu.blp:227
#: resources/ui/performance_page/memory.blp:160
#: resources/ui/performance_page/network.blp:129
msgid "CP_U"
msgstr "_Suoritin"

#: resources/ui/performance_page/cpu.blp:176
#: resources/ui/performance_page/disk.blp:192
#: resources/ui/performance_page/gpu.blp:232
#: resources/ui/performance_page/memory.blp:165
#: resources/ui/performance_page/network.blp:134
msgid "_Memory"
msgstr "_Muisti"

#: resources/ui/performance_page/cpu.blp:181
#: resources/ui/performance_page/disk.blp:197
#: resources/ui/performance_page/gpu.blp:237
#: resources/ui/performance_page/memory.blp:170
#: resources/ui/performance_page/network.blp:139
msgid "_Disk"
msgstr "_Levy"

#: resources/ui/performance_page/cpu.blp:186
#: resources/ui/performance_page/disk.blp:202
#: resources/ui/performance_page/gpu.blp:242
#: resources/ui/performance_page/memory.blp:175
#: resources/ui/performance_page/network.blp:144
msgid "_Network"
msgstr "_Verkko"

#: resources/ui/performance_page/cpu.blp:191
#: resources/ui/performance_page/disk.blp:207
#: resources/ui/performance_page/gpu.blp:247
#: resources/ui/performance_page/memory.blp:180
#: resources/ui/performance_page/network.blp:149
msgid "_GPU"
msgstr "_Näytönohjain"

#: resources/ui/performance_page/cpu.blp:199
#: resources/ui/performance_page/disk.blp:215
#: resources/ui/performance_page/gpu.blp:255
#: resources/ui/performance_page/memory.blp:188
#: resources/ui/performance_page/network.blp:162
msgid "_Copy"
msgstr "_Kopioi"

#: resources/ui/performance_page/cpu_details.blp:68
msgid "Speed"
msgstr "Nopeus"

#: resources/ui/performance_page/cpu_details.blp:95 src/apps_page/mod.rs:529
msgid "Processes"
msgstr "Prosessit"

#: resources/ui/performance_page/cpu_details.blp:117
msgid "Threads"
msgstr "Säikeet"

#: resources/ui/performance_page/cpu_details.blp:139
msgid "Handles"
msgstr "Kahvat"

#: resources/ui/performance_page/cpu_details.blp:162
msgid "Up time"
msgstr "Toiminta-aika"

#: resources/ui/performance_page/cpu_details.blp:188
msgid "Base Speed:"
msgstr "Perusnopeus:"

#: resources/ui/performance_page/cpu_details.blp:197
msgid "Sockets:"
msgstr "Kantoja:"

#: resources/ui/performance_page/cpu_details.blp:206
msgid "Virtual processors:"
msgstr "Virtuaalisia suorittimia:"

#: resources/ui/performance_page/cpu_details.blp:215
msgid "Virtualization:"
msgstr "Virtualisointi:"

#: resources/ui/performance_page/cpu_details.blp:224
msgid "Virtual machine:"
msgstr "Virtuaalikone:"

#: resources/ui/performance_page/cpu_details.blp:233
msgid "L1 cache:"
msgstr "L1-välimuisti:"

#: resources/ui/performance_page/cpu_details.blp:242
msgid "L2 cache:"
msgstr "L2-välimuisti:"

#: resources/ui/performance_page/cpu_details.blp:251
msgid "L3 cache:"
msgstr "L3-välimuisti:"

#: resources/ui/performance_page/disk.blp:64
#: resources/ui/performance_page/disk_details.blp:46
msgid "Active time"
msgstr "Aktiivisuusaika"

#: resources/ui/performance_page/disk.blp:123
msgid "Disk transfer rate"
msgstr "Levyn siirtonopeus"

#: resources/ui/performance_page/disk_details.blp:69
msgid "Avg. response time"
msgstr "Keskimääräinen vasteaika"

#: resources/ui/performance_page/disk_details.blp:105
msgid "Read speed"
msgstr "Lukunopeus"

#: resources/ui/performance_page/disk_details.blp:137
msgid "Write speed"
msgstr "Kirjoitusnopeus"

#: resources/ui/performance_page/disk_details.blp:165
msgid "Capacity:"
msgstr "Kapasiteetti:"

#: resources/ui/performance_page/disk_details.blp:174
msgid "Formatted:"
msgstr "Alustettu:"

#: resources/ui/performance_page/disk_details.blp:183
msgid "System disk:"
msgstr "Järjestelmälevy:"

#: resources/ui/performance_page/disk_details.blp:192
#: resources/ui/performance_page/memory_details.blp:225
msgid "Type:"
msgstr "Tyyppi:"

#: resources/ui/performance_page/gpu.blp:71
msgid "Overall utilization"
msgstr "Käyttö yhteensä"

#: resources/ui/performance_page/gpu.blp:111
msgid "Video encode"
msgstr "Videon enkoodaus"

#: resources/ui/performance_page/gpu.blp:147
msgid "Video decode"
msgstr "Videon dekoodaus"

#: resources/ui/performance_page/gpu.blp:186
#: resources/ui/performance_page/gpu_details.blp:157
#: resources/ui/performance_page/memory.blp:65
msgid "Memory usage"
msgstr "Muistinkulutus"

#: resources/ui/performance_page/gpu_details.blp:67
msgid "Clock Speed"
msgstr "Kellotaajuus"

#: resources/ui/performance_page/gpu_details.blp:112
msgid "Power draw"
msgstr "Virrankulutus"

#: resources/ui/performance_page/gpu_details.blp:202
msgid "Memory speed"
msgstr "Muistin nopeus"

#: resources/ui/performance_page/gpu_details.blp:247
msgid "Temperature"
msgstr "Lämpötila"

#: resources/ui/performance_page/gpu_details.blp:275
msgid "OpenGL version:"
msgstr "OpenGL-versio:"

#: resources/ui/performance_page/gpu_details.blp:284
msgid "Vulkan version:"
msgstr "Vulkan-versio:"

#: resources/ui/performance_page/gpu_details.blp:293
msgid "PCI Express speed:"
msgstr "PCI Express -nopeus:"

#: resources/ui/performance_page/gpu_details.blp:302
msgid "PCI bus address:"
msgstr "PCI-väylän osoite:"

#: resources/ui/performance_page/memory.blp:44 src/apps_page/mod.rs:1174
#: src/performance_page/mod.rs:427
msgid "Memory"
msgstr "Muisti"

#: resources/ui/performance_page/memory.blp:126
msgid "Memory composition"
msgstr "Muistin kulutus"

#: resources/ui/performance_page/memory_details.blp:47
msgid "In use"
msgstr "Käytössä"

#: resources/ui/performance_page/memory_details.blp:70
msgid "Available"
msgstr "Saatavilla"

#: resources/ui/performance_page/memory_details.blp:98
msgid "Committed"
msgstr "Kommitoitu"

#: resources/ui/performance_page/memory_details.blp:121
msgid "Cached"
msgstr "Välimuistissa"

#: resources/ui/performance_page/memory_details.blp:148
msgid "Swap available"
msgstr "Swap-tilaa saatavilla"

#: resources/ui/performance_page/memory_details.blp:171
msgid "Swap used"
msgstr "Swap-tilaa käytetty"

#: resources/ui/performance_page/memory_details.blp:198
msgid "Speed:"
msgstr "Nopeus:"

#: resources/ui/performance_page/memory_details.blp:207
msgid "Slots used:"
msgstr "Paikkoja käytetty:"

#: resources/ui/performance_page/memory_details.blp:216
msgid "Form factor:"
msgstr "Kantakoko:"

#: resources/ui/performance_page/network.blp:62
msgid "Throughput"
msgstr "Välityskyky"

#: resources/ui/performance_page/network.blp:155
msgid "Network Se_ttings"
msgstr "_Verkkoasetukset"

#: resources/ui/performance_page/network_details.blp:51
msgid "Receive"
msgstr "Vastaanotto"

#: resources/ui/performance_page/network_details.blp:83
msgid "Send"
msgstr "Lähetys"

#: resources/ui/performance_page/network_details.blp:110
msgid "Interface name:"
msgstr "Liitännän nimi:"

#: resources/ui/performance_page/network_details.blp:119
msgid "Connection type:"
msgstr "Yhteyden tyyppi:"

#: resources/ui/performance_page/network_details.blp:129
msgid "SSID:"
msgstr "SSID:"

#: resources/ui/performance_page/network_details.blp:139
msgid "Signal strength:"
msgstr "Signaalin voimakkuus:"

#: resources/ui/performance_page/network_details.blp:149
msgid "Maximum Bitrate:"
msgstr "Enimmäisnopeus;:"

#: resources/ui/performance_page/network_details.blp:159
msgid "Frequency:"
msgstr "Taajuus:"

#: resources/ui/performance_page/network_details.blp:168
msgid "Hardware address:"
msgstr "Laiteosoite:"

#: resources/ui/performance_page/network_details.blp:251
msgid "IPv4 address:"
msgstr "IPv4-osoite:"

#: resources/ui/performance_page/network_details.blp:260
msgid "IPv6 address:"
msgstr "IPv6-osoite:"

#: resources/ui/preferences/page.blp:6
msgid "General Settings"
msgstr "Yleiset asetukset"

#: resources/ui/preferences/page.blp:9
msgid "Update Speed"
msgstr "Päivitysnopeus"

#: resources/ui/preferences/page.blp:12 src/preferences/page.rs:113
#: src/preferences/page.rs:262
msgid "Fast"
msgstr "Nopea"

#: resources/ui/preferences/page.blp:13
msgid "Refresh every half second"
msgstr "Päivitä puolen sekunnin välein"

#: resources/ui/preferences/page.blp:17 src/preferences/page.rs:109
#: src/preferences/page.rs:122 src/preferences/page.rs:266
#: src/preferences/page.rs:282
msgid "Normal"
msgstr "Tavallinen"

#: resources/ui/preferences/page.blp:18
msgid "Refresh every second"
msgstr "Päivitä sekunnin välein"

#: resources/ui/preferences/page.blp:22 src/preferences/page.rs:105
#: src/preferences/page.rs:270
msgid "Slow"
msgstr "Hidas"

#: resources/ui/preferences/page.blp:23
msgid "Refresh every second and a half"
msgstr "Päivitä 1,5 sekunnin välein"

#: resources/ui/preferences/page.blp:27 src/preferences/page.rs:101
#: src/preferences/page.rs:274
msgid "Very Slow"
msgstr "Erittäin hidas"

#: resources/ui/preferences/page.blp:28
msgid "Refresh every 2 seconds"
msgstr "Päivitä kahden sekunnin välein"

#: resources/ui/preferences/page.blp:34
msgid "App Page Settings"
msgstr "Sovellussivun asetukset"

#: resources/ui/preferences/page.blp:37
msgid "Merge Process Stats"
msgstr "Yhdistä prosessitilastot"

#: resources/ui/preferences/page.blp:42
msgid "Remember Sorting"
msgstr "Muista järjestys"

#: resources/ui/preferences/page.blp:43
msgid "Remember the sorting of the app and process list across app restarts"
msgstr ""
"Muista sovellus- ja prosessiluettelon järjestys sovelluksen "
"uudelleenkäynnistyksen välillä"

#: resources/ui/window.blp:43
msgid "Resources"
msgstr "Resurssit"

#: resources/ui/window.blp:69
msgid "Toggle Sidebar"
msgstr "Näytä/piilota sivupalkki"

#: resources/ui/window.blp:92
msgid "Type a name or PID to search"
msgstr "Kirjoita nimi tai PID etsiäksesi"

#: resources/ui/window.blp:148
msgid "Loading..."
msgstr "Ladataan..."

#: resources/ui/window.blp:162
msgid "Performance"
msgstr "Suorituskyky"

#: resources/ui/window.blp:173 src/apps_page/mod.rs:522
msgid "Apps"
msgstr "Sovellukset"

#: resources/ui/window.blp:192
msgid "_Preferences"
msgstr "_Asetukset"

#: resources/ui/window.blp:197
msgid "_About MissionCenter"
msgstr "_Tietoja - Tehtäväkeskus"

#: src/apps_page/mod.rs:1156
msgid "Name"
msgstr "Nimi"

#: src/apps_page/mod.rs:1162
msgid "PID"
msgstr "PID"

#: src/apps_page/mod.rs:1180
msgid "Disk"
msgstr "Levy"

#: src/apps_page/mod.rs:1187
msgid "GPU"
msgstr "Näytönohjain"

#. ContentType::App
#: src/apps_page/list_item.rs:315
msgid "Stop Application"
msgstr "Pysäytä sovellus"

#: src/apps_page/list_item.rs:315
msgid "Force Stop Application"
msgstr "Pakota sovelluksen pysäytys"

#. ContentType::Process
#: src/apps_page/list_item.rs:319
msgid "Stop Process"
msgstr "Pysäytä prosessi"

#: src/apps_page/list_item.rs:319
msgid "Force Stop Process"
msgstr "Pakota prosessin pysäytys"

#: src/performance_page/widgets/mem_composition_widget.rs:285
msgid ""
"In use ({}B)\n"
"\n"
"Memory used by the operating system and running applications"
msgstr ""
"Käytössä ({} B)\n"
"\n"
"Muistin käyttö käyttöjärjestelmän ja käynnissä olevien sovellusten toimesta"

#: src/performance_page/widgets/mem_composition_widget.rs:317
msgid ""
"Modified ({}B)\n"
"\n"
"Memory whose contents must be written to disk before it can be used by "
"another process"
msgstr ""
"Muokattu ({} t)\n"
"\n"
"Muisti, jonka sisältö tulee kirjoittaa levylle, ennen kuin sitä on "
"mahdollista käyttää toisen prosessin toimesta"

#: src/performance_page/widgets/mem_composition_widget.rs:337
msgid ""
"Standby ({}B)\n"
"\n"
"Memory that contains cached data and code that is not actively in use"
msgstr ""
"Lepo ({} t)\n"
"\n"
"Muisti, joka sisältää välimuistiin asetettua dataa ja koodia, joka ei ole "
"aktiivisessa käytössä"

#: src/performance_page/widgets/mem_composition_widget.rs:350
msgid ""
"Free ({}B)\n"
"\n"
"Memory that is not currently in use, and that will be repurposed first when "
"the operating system, drivers, or applications need more memory"
msgstr ""
"Vapaa ({} t)\n"
"\n"
"Muisti, joka ei tällä hetkellä ole käytössä, ja otetaan käyttöön, kun "
"käyttöjärjestelmä, ajurit tai sovellukset tarvitsevat muistia"

#: src/performance_page/mod.rs:489 src/performance_page/disk.rs:196
msgid "Disk {} ({})"
msgstr "Levy {} ({})"

#: src/performance_page/mod.rs:493
msgid "HDD"
msgstr "HDD"

#: src/performance_page/mod.rs:494
msgid "SSD"
msgstr "SSD"

#: src/performance_page/mod.rs:495
msgid "NVMe"
msgstr "NVMe"

#: src/performance_page/mod.rs:496
msgid "eMMC"
msgstr "eMMC"

#: src/performance_page/mod.rs:497
msgid "iSCSI"
msgstr "iSCSI"

#: src/performance_page/mod.rs:498 src/performance_page/cpu.rs:305
#: src/performance_page/cpu.rs:321 src/performance_page/cpu.rs:333
#: src/performance_page/cpu.rs:341 src/performance_page/disk.rs:301
#: src/performance_page/gpu.rs:300 src/performance_page/gpu.rs:417
#: src/performance_page/memory.rs:261 src/performance_page/network.rs:392
#: src/performance_page/network.rs:416 src/performance_page/network.rs:426
#: src/performance_page/network.rs:465 src/performance_page/network.rs:515
#: src/performance_page/network.rs:551
msgid "Unknown"
msgstr "Tuntematon"

#: src/performance_page/mod.rs:577 src/performance_page/network.rs:331
msgid "Ethernet"
msgstr "Ethernet"

#: src/performance_page/mod.rs:578 src/performance_page/network.rs:346
msgid "Wi-Fi"
msgstr "Wi-Fi"

#: src/performance_page/mod.rs:579 src/performance_page/network.rs:348
msgid "Other"
msgstr "Muu"

#: src/performance_page/mod.rs:678
msgid "GPU {}"
msgstr "Näytönohjain {}"

#: src/performance_page/mod.rs:853 src/performance_page/mod.rs:861
msgid "{}: {} {}bps"
msgstr "{}: {} {} bps"

#: src/performance_page/cpu.rs:316
msgid "Supported"
msgstr "Tuettu"

#: src/performance_page/cpu.rs:318 src/performance_page/gpu.rs:312
msgid "Unsupported"
msgstr "Ei tuettu"

#: src/performance_page/cpu.rs:328 src/performance_page/disk.rs:232
msgid "Yes"
msgstr "Kyllä"

#: src/performance_page/cpu.rs:330 src/performance_page/disk.rs:234
msgid "No"
msgstr "Ei"

#: src/performance_page/cpu.rs:869
msgid "Utilization over {} seconds"
msgstr "Käyttö {} sekunnin aikana"

#: src/performance_page/cpu.rs:873 src/performance_page/disk.rs:480
#: src/performance_page/memory.rs:568 src/performance_page/network.rs:734
msgid "{} seconds"
msgstr "{} sekuntia"

#: src/performance_page/disk.rs:263
msgid "{} {}{}B/s"
msgstr "{} {}{} t/s"

#: src/performance_page/gpu.rs:265
msgid "Video encode/decode"
msgstr "Videon pakkaus/purku"

#: src/performance_page/memory.rs:477
msgid "Authentication failed"
msgstr "Tunnistautuminen epäonnistui"

#: src/performance_page/memory.rs:483
msgid "More info"
msgstr "Lisätietoja"

#: src/performance_page/network.rs:438 src/performance_page/network.rs:445
#: src/performance_page/network.rs:455
msgid "{} {}bps"
msgstr "{} {} bps"

#: src/performance_page/network.rs:477 src/performance_page/network.rs:492
msgid "N/A"
msgstr "-"

#: src/application.rs:272
msgid "translator-credits"
msgstr "Jiri Grönroos"

#~ msgid "Fixes:"
#~ msgstr "Korjaukset:"

#~ msgid "Translations:"
#~ msgstr "Käännökset:"

#~ msgid "First official release!"
#~ msgstr "Ensimmäinen virallinen julkaisu!"

#~ msgid "Some information requires administrative privileges"
#~ msgstr "Jotkin tiedot vaativat ylläpitäjän oikeuksia"

#~ msgid "% Utilization"
#~ msgstr "%-käyttö"

#~ msgid "{}: {} {}bps {}: {} {}bps"
#~ msgstr "{}: {} {} bps {}: {} {} bps"

#~ msgid "% Utilization over {} seconds"
#~ msgstr "%-käyttö {} sekunnin aikana"

#~ msgid "_Quit"
#~ msgstr "_Lopeta"
